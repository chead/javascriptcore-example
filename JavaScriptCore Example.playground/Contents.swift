import Foundation
import JavaScriptCore

let context = JSContext()!

context.setObject(Animal.self,
                  forKeyedSubscript: "Animal" as (NSCopying & NSObjectProtocol))

context.setObject(Dog.self,
                  forKeyedSubscript: "Dog" as (NSCopying & NSObjectProtocol))

context.setObject(Cat.self,
                  forKeyedSubscript: "Cat" as (NSCopying & NSObjectProtocol))

let scenePath = Bundle.main.url(forResource: "Scene",
                                withExtension: "js")!

let sceneScript = try? String.init(contentsOf: scenePath,
                                   encoding: .utf8)

let _ = context.evaluateScript(sceneScript)

let animals = [context.objectForKeyedSubscript("animal").toObjectOf(Animal.self) as! Animal,
               context.objectForKeyedSubscript("dog1").toObjectOf(Dog.self)      as! Dog,
               context.objectForKeyedSubscript("dog2").toObjectOf(Dog.self)      as! Dog,
               context.objectForKeyedSubscript("cat").toObjectOf(Cat.self)       as! Cat]

print("Animal type:")

for animal in animals { print("\t\(type(of: animal))") }

print("Vocalization:")

for animal in animals { print("\t" + animal.vocalization()) }

print("Dog names:")

for dog in (animals.filter { $0 is Dog }) {
    print("\t" + dog.name)
}
