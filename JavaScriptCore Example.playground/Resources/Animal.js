"use strict";

class Animal {
    constructor(name) {
        this.name = name;
    }

    vocalize() {
        console.log(this.name
        + ' '
        + this.constructor.name.toLowerCase() +
        ' makes a noise.');
    }
}
