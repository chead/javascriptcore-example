"use strict";

export default class Cat extends Animal {
    vocalize() {
        console.log(this.name + ' the ' + this.constructor.name.toLowerCase() + ' meows.');
    }
}
