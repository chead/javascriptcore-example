"use strict";

export default class Dog extends Animal {
    vocalize() {
        console.log(this.name + ' the ' + this.constructor.name.toLowerCase() + ' barks.');
    }
}
