import Foundation

@objc public class Animal: NSObject, AnimalRepresenting {
    @objc public dynamic var name: String

    public init(name: String) {
        self.name = name
    }

    public class func animalWithName(_ name: String) -> Animal {
        return Animal(name: name)
    }

    public func vocalization() -> String {
        return "vocalization"
    }
}
