import Foundation
import JavaScriptCore

@objc public protocol AnimalRepresenting: JSExport {
    var name: String { get set }

    static func animalWithName(_ name: String) -> Animal
    
    func vocalization() -> String
}
