import Foundation

@objc public class Cat: Animal, CatRepresenting {
    override public class func animalWithName(_ name: String) -> Animal {
        return Cat(name: name)
    }

    override public func vocalization() -> String {
        return "meow"
    }
}
