import Foundation

@objc public class Dog: Animal, DogRepresenting {
    override public class func animalWithName(_ name: String) -> Animal {
        return Dog(name: name)
    }

    override public func vocalization() -> String {
        return "bark"
    }
}

