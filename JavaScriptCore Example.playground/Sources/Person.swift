import Foundation

@objc public class Person: NSObject, PersonRepresenting {
    @objc dynamic public var name: String

    public init(name: String) {
        self.name = name
    }

    public class func personWithName(_ name: String) -> Person {
        return Person(name: name)
    }
}
