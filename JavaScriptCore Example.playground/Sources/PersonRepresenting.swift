import Foundation
import JavaScriptCore

@objc public protocol PersonRepresenting: JSExport {
    var name: String { get set }

    static func personWithName(_ name: String) -> Person
}
